module.exports = {
    en: {
        translation: {
            WELCOME_MSG: `Welcome to Holiday times `,
            WELCOME_BACK_MSG: 'Welcome back! ',
            REJECTED_MSG: 'No problem. Please say the date again so I can get it right.',
            DAYS_LEFT_MSG: `There's {{count}} day left `,
            DAYS_LEFT_MSG_plural: 'There are {{count}} days left ',
            WILL_TURN_MSG: `for your holiday `,
            WILL_TURN_MSG_plural: `for your holidays. `,
            GREET_MSG: `Hurrah! Happy holiday `,
            GREET_MSG_plural: `Happy holidays `,
            MISSING_MSG: `It looks like you haven't told me your holiday yet. `,
            POST_SAY_HELP_MSG: `If you want to change the date, try saying, register my holiday. Or just say the date directly. What would you like to do next? `,
            HELP_MSG: 'I can remember your holiday if you tell me the date. Or I can tell you the remaining days until your next holiday. Which one would you like to try? ',
            REPROMPT_MSG: `If you're not sure what to do next try asking for help. If you want to leave just say stop. What would you like to do next? `,
            GOODBYE_MSG: 'Goodbye!',
            REFLECTOR_MSG: 'You just triggered {{intent}}',
            FALLBACK_MSG: 'Sorry, I don\'t know about that. Please try again.',
            ERROR_MSG: 'Sorry, there was an error. Please try again.'
        }
    }
}