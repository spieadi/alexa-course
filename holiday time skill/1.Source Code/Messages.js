module.exports = {
    en: {
        translation: {
            WELCOME_MSG: `Welcome to Holiday time. `,
            REGISTER_MSG: 'Your holiday is on {{month}} {{day}} {{year}}.',
            REJECTED_MSG: 'No problem. Please say the date again so I can get it right.',
            HELP_MSG: `You can tell me your date of holiday and I'll take note. You can also just say, register my holiday and I will guide you. Which one would you like to try?`,
            GOODBYE_MSG: 'Goodbye!',
            REFLECTOR_MSG: 'You just triggered {{intent}}',
            FALLBACK_MSG: 'Sorry, I don\'t know about that. Please try again.',
            ERROR_MSG: 'Sorry, there was an error. Please try again.'
        }
    }
}